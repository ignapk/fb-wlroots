Framebuffer wlroots backend
===========================

Framebuffer backend based on [implementation][1] by ollieparanoid for [wlroots][2].
Continuation of [previous][3] effort by blacksilver.
As it uses internal unstable wlroots api, the version sticks to wlroots.

## Usage

Just call `wlr_backend_autocreate_ext` instead of `wlr_backend_autocreate`.

    #include <fb-wlr/fb-backend.h>
    ...
    self->backend = wlr_backend_autocreate_ext(self->wl_display, NULL); 

The fbdev backend will only be used if the drm backend fails, to change this behaviour and run compositor with this backend, set the environmental variable `WLR_BACKENDS` to `fbdev`.

    WLR_BACKENDS=fbdev compositor

## Building

    meson setup build
    meson compile -C build
    sudo meson install -C build

[1]: https://wiki.postmarketos.org/wiki/User:Ollieparanoid/Run_wlroots_with_fbdev
[2]: https://gitlab.freedesktop.org/wlroots/wlroots
[3]: https://gitlab.com/blacksilver/fb-wlroots
