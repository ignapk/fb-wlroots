#ifndef FB_WLR_GLES2_H
#define FB_WLR_GLES2_H

#include <wlr/render/egl.h>
#include <wlr/render/wlr_renderer.h>

struct wlr_egl *fb_wlr_gles2_renderer_get_egl(struct wlr_renderer *renderer);

#endif
