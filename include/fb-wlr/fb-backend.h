#ifndef FB_WLR_BACKEND_H
#define FB_WLR_BACKEND_H

#include <wlr/backend.h>

struct wlr_backend *wlr_backend_autocreate_ext(struct wl_display *display);

#endif
