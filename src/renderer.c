/*#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <wlr/render/egl.h>
#include <wlr/render/gles2.h>
#include <wlr/render/interface.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/util/log.h>*/

#include <wlr/util/log.h>
#include <wlr/render/egl.h>
#include "gles2/gles2.h"
#include "renderer.h"

struct wlr_renderer *wlr_fbdev_renderer_autocreate(void) {
	struct wlr_egl *egl = wlr_egl_create(EGL_PLATFORM_SURFACELESS_MESA,
			EGL_DEFAULT_DISPLAY);
	if (egl == NULL) {
		wlr_log(WLR_ERROR, "Could not initialize EGL");
		return NULL;
	}

	struct wlr_renderer *renderer = wlr_fbdev_gles2_renderer_create(egl);
	if (!renderer) {
		wlr_log(WLR_ERROR, "Failed to create GLES2 renderer");
		wlr_egl_destroy(egl);
	}

	return renderer;
}
