#ifndef FB_WLR_BACKEND_RENDERER_H
#define FB_WLR_BACKEND_RENDERER_H

#include <wlr/render/wlr_renderer.h>

struct wlr_renderer *wlr_fbdev_renderer_autocreate(void);

#endif
