#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <libudev.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/backend/session.h>
#include <wlr/config.h>
#include <wlr/util/log.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include "session.h"
#include "signal.h"

#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define WAIT_FBDEV_TIMEOUT 10000 // ms

static bool is_fbdev(const char *sysname) {
	const char prefix[] = "fb";
	if (strncmp(sysname, prefix, strlen(prefix)) != 0) {
		return false;
	}
	for (size_t i = strlen(prefix); sysname[i] != '\0'; i++) {
		if (sysname[i] < '0' || sysname[i] > '9') {
			return false;
		}
	}
	return true;
}

static int udev_event(int fd, uint32_t mask, void *data) {
	struct wlr_session_ex *session_ex = data;
	struct wlr_session *session = session_ex->session;

	struct udev_device *udev_dev = udev_monitor_receive_device(session->mon);
	if (!udev_dev) {
		return 1;
	}

	const char *sysname = udev_device_get_sysname(udev_dev);
	const char *devnode = udev_device_get_devnode(udev_dev);
	const char *action = udev_device_get_action(udev_dev);
	wlr_log(WLR_DEBUG, "my udev event for %s (%s)", sysname, action);

	if (!is_fbdev(sysname) || !action || !devnode) {
		goto out;
	}

	const char *seat = udev_device_get_property_value(udev_dev, "ID_SEAT");
	if (!seat) {
		seat = "seat0";
	}
	if (session->seat[0] != '\0' && strcmp(session->seat, seat) != 0) {
		goto out;
	}

	if (strcmp(action, "add") == 0) {
		wlr_log(WLR_DEBUG, "FBDEV device %s added", sysname);
		struct wlr_session_add_event event = {
			.path = devnode,
		};
		wlr_signal_emit_safe(&session_ex->events.add_fbdev, &event);
	}
out:
	udev_device_unref(udev_dev);
	return 1;
}

static struct wlr_device *open_fbdev(struct wlr_session *restrict session,
		const char *restrict path) {
	if (!path) {
		return NULL;
	}

	struct wlr_device *dev = wlr_session_open_file(session, path);
	if (!dev) {
		return NULL;
	}

	struct fb_fix_screeninfo scr_fix;
	if (ioctl(dev->fd, FBIOGET_FSCREENINFO, &scr_fix) == -1) {
		wlr_log(WLR_ERROR, "%s: FBIOGET_FSCREENINFO failed", path);
		goto out_dev;
	}

	// Skip fbdevs that can't be opened with mmap (as seen on a Nexus 4
	// downstream kernel)
	size_t fbmem_size = 1;
	unsigned char *fbmem = mmap(NULL, fbmem_size, PROT_WRITE, MAP_SHARED, dev->fd, 0);
	if (fbmem == MAP_FAILED) {
		wlr_log(WLR_ERROR, "%s: mmap failed", path);
		goto out_dev;
	}

	munmap(fbmem, fbmem_size);
	return dev;

out_dev:
	wlr_session_close_file(session, dev);
	return NULL;
}

static ssize_t explicit_find_fbdevs(struct wlr_session *session,
		size_t ret_len, struct wlr_device *ret[static ret_len], const char *str) {
	char *fbdevs = strdup(str);
	if (!fbdevs) {
		wlr_log_errno(WLR_ERROR, "Allocation failed");
		return -1;
	}

	size_t i = 0;
	char *save;
	char *ptr = strtok_r(fbdevs, ":", &save);
	do {
		if (i >= ret_len) {
			break;
		}

		ret[i] = open_fbdev(session, ptr);
		if (!ret[i]) {
			wlr_log(WLR_ERROR, "Unable to open %s as fbdev device",
				ptr);
		} else {
			++i;
		}
	} while ((ptr = strtok_r(NULL, ":", &save)));

	free(fbdevs);
	return i;
}

static struct udev_enumerate *enumerate_fbdevs(struct udev *udev) {
	struct udev_enumerate *en = udev_enumerate_new(udev);
	if (!en) {
		wlr_log(WLR_ERROR, "udev_enumerate_new failed");
		return NULL;
	}

	udev_enumerate_add_match_subsystem(en, "graphics");
	udev_enumerate_add_match_sysname(en, "fb[0-9]*");

	if (udev_enumerate_scan_devices(en) != 0) {
		wlr_log(WLR_ERROR, "udev_enumerate_scan_devices failed");
		udev_enumerate_unref(en);
		return NULL;
	}

	return en;
}

static uint64_t get_current_time_ms(void) {
	struct timespec ts = {0};
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000 + (uint64_t)ts.tv_nsec / 1000000;
}

struct find_fbdevs_add_handler {
	bool added;
	struct wl_listener listener;
};

static void find_fbdevs_handle_add(struct wl_listener *listener, void *data) {
	struct find_fbdevs_add_handler *handler =
		wl_container_of(listener, handler, listener);
	handler->added = true;
}

/* Tries to find the primary fbdev by checking for the "boot_vga" attribute.
 * If it's not found, it returns the first valid dev it finds.
 */
ssize_t wlr_session_find_fbdevs(struct wlr_session *session,
                                      size_t ret_len, struct wlr_device **ret) {
	struct wlr_session_ex session_ex = {
		.session = session,
	};
	wl_signal_init(&session_ex.events.add_fbdev);

	udev_monitor_filter_add_match_subsystem_devtype(session->mon, "graphics", NULL);
	udev_monitor_enable_receiving(session->mon);
	struct wl_event_loop *event_loop = wl_display_get_event_loop(session->display);
	int fd = udev_monitor_get_fd(session->mon);
	struct wl_event_source *my_udev_event = wl_event_loop_add_fd(event_loop, fd,
			WL_EVENT_READABLE, udev_event, &session_ex);
	if (!my_udev_event) {
		wlr_log_errno(WLR_ERROR, "Failed to create udev event source");
	}



	const char *explicit = getenv("WLR_FBDEV_DEVICES");
	if (explicit) {
		return explicit_find_fbdevs(session, ret_len, ret, explicit);
	}

	struct udev_enumerate *en = enumerate_fbdevs(session->udev);
	if (!en) {
		return -1;
	}

	if (udev_enumerate_get_list_entry(en) == NULL) {
		udev_enumerate_unref(en);
		wlr_log(WLR_INFO, "Waiting for a fbdev device");

		struct find_fbdevs_add_handler handler = {0};
		handler.listener.notify = find_fbdevs_handle_add;
		wl_signal_add(&session_ex.events.add_fbdev, &handler.listener);

		uint64_t started_at = get_current_time_ms();
		uint64_t timeout = WAIT_FBDEV_TIMEOUT;
		while (!handler.added) {
			int ret = wl_event_loop_dispatch(event_loop, (int)timeout);
			if (ret < 0) {
				wlr_log_errno(WLR_ERROR, "Failed to wait for a fbdev device: "
						"wl_event_loop_dispatch failed");
				udev_enumerate_unref(en);
				return -1;
			}

			uint64_t now = get_current_time_ms();
			if (now >= started_at + WAIT_FBDEV_TIMEOUT) {
				break;
			}
			timeout = started_at + WAIT_FBDEV_TIMEOUT - now;
		}

		wl_list_remove(&handler.listener.link);

		en = enumerate_fbdevs(session->udev);
		if (!en) {
			return -1;
		}
	}

	struct udev_list_entry *entry;
	size_t i = 0;

	udev_list_entry_foreach(entry, udev_enumerate_get_list_entry(en)) {
		if (i == ret_len) {
			break;
		}

		bool is_boot_vga = false;

		const char *path = udev_list_entry_get_name(entry);
		struct udev_device *dev = udev_device_new_from_syspath(session->udev, path);
		if (!dev) {
			continue;
		}

		const char *seat = udev_device_get_property_value(dev, "ID_SEAT");
		if (!seat) {
			seat = "seat0";
		}
		if (session->seat[0] && strcmp(session->seat, seat) != 0) {
			udev_device_unref(dev);
			continue;
		}

		// This is owned by 'dev', so we don't need to free it
		struct udev_device *pci =
			udev_device_get_parent_with_subsystem_devtype(dev, "pci", NULL);

		if (pci) {
			const char *id = udev_device_get_sysattr_value(pci, "boot_vga");
			if (id && strcmp(id, "1") == 0) {
				is_boot_vga = true;
			}
		}

		struct wlr_device *wlr_dev = open_fbdev(session, udev_device_get_devnode(dev));
		if (!wlr_dev) {
			udev_device_unref(dev);
			continue;
		}

		udev_device_unref(dev);

		ret[i] = wlr_dev;
		if (is_boot_vga) {
			struct wlr_device *tmp = ret[0];
			ret[0] = ret[i];
			ret[i] = tmp;
		}

		++i;
	}

	udev_enumerate_unref(en);

	wl_event_source_remove(my_udev_event);
	return i;
}
