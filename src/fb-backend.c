#define _POSIX_C_SOURCE 200809L
#include <wlr/backend/interface.h>
#include <wlr/backend/multi.h>
#include <wlr/backend/libinput.h>
#include <wlr/backend.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_output.h>
#warning Missing wlr-API for setting session. Might be obsolete after upgrade.
#include "backend/multi.h"
#include "backend/fbdev.h"
#include "fbdev.h"
#include "fb-wlr/fb-backend.h"
#include "session.h"

static struct wlr_backend *attempt_fbdev_backend(struct wl_display *display,
		struct wlr_session *session) {
	struct wlr_device *fbdevs[8];
	ssize_t num_fbdevs = wlr_session_find_fbdevs(session, 8, fbdevs);
	if (num_fbdevs < 0) {
		wlr_log(WLR_ERROR, "Failed to find FBDEVs");
		return NULL;
	}

	wlr_log(WLR_INFO, "Found %zu framebuffer devices", num_fbdevs);

	struct wlr_backend *primary_fbdev = NULL;
	for (size_t i = 0; i < (size_t)num_fbdevs; ++i) {
		struct wlr_backend *fbdev = wlr_fbdev_backend_create(display,
			session, fbdevs[i], primary_fbdev);
		if (!fbdev) {
			wlr_log(WLR_ERROR, "Failed to create FBDEV backend");
			continue;
		}

		if (!primary_fbdev) {
			primary_fbdev = fbdev;
		}

		// Each framebuffer backend has one output
		wlr_fbdev_add_output(fbdev, 0, 0);
	}

	return primary_fbdev;
}

static struct wlr_backend *attempt_backend_ext(struct wl_display *display) {
	struct wlr_backend *backend = wlr_multi_backend_create(display);
	struct wlr_multi_backend *multi = (struct wlr_multi_backend *)backend;
	if (!backend) {
		wlr_log(WLR_ERROR, "could not allocate multibackend");
		return NULL;
	}

	multi->session = wlr_session_create(display);
	if (!multi->session) {
		wlr_log(WLR_ERROR, "failed to start a session");
  wlr_backend_destroy(backend);
		return NULL;
	}

  struct wlr_backend *subbackend = wlr_libinput_backend_create(display, multi->session);
	if (subbackend == NULL) {
		wlr_log(WLR_ERROR, "failed to start backend libinput");
		wlr_session_destroy(multi->session);
		wlr_backend_destroy(backend);
		return NULL;
	}
	if (!wlr_multi_backend_add(backend, subbackend)) {
		wlr_log(WLR_ERROR, "failed to add backend libinput");
		wlr_session_destroy(multi->session);
		wlr_backend_destroy(subbackend);
		wlr_backend_destroy(backend);
		return NULL;
	}
	subbackend = attempt_fbdev_backend(display, multi->session);
	if (subbackend == NULL) {
		wlr_log(WLR_ERROR, "failed to start backend fbdev");
		wlr_session_destroy(multi->session);
		wlr_backend_destroy(backend);
		return NULL;
	}

	if (!wlr_multi_backend_add(backend, subbackend)) {
		wlr_log(WLR_ERROR, "failed to add backend fbdev");
		wlr_session_destroy(multi->session);
		wlr_backend_destroy(backend);
		return NULL;
	}
	return backend;
}

struct wlr_backend *wlr_backend_autocreate_ext(struct wl_display *display) {
	struct wlr_backend *backend;
	char *names = getenv("WLR_BACKENDS");
	if (names && strstr(names, "fbdev")) {
		backend = attempt_backend_ext(display);
	} else {
		backend = wlr_backend_autocreate(display);
		if (backend == NULL) {
			backend = attempt_backend_ext(display);
		}
	}
	return backend;
}

