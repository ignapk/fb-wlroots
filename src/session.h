#ifndef FB_WLR_BACKEND_SESSION_H
#define FB_WLR_BACKEND_SESSION_H

#include <wlr/backend/session.h>

ssize_t wlr_session_find_fbdevs(struct wlr_session *session,
	size_t ret_len, struct wlr_device **ret);

struct wlr_session_ex {
	struct wlr_session *session;

	struct {
		struct wl_signal add_fbdev; // struct wlr_session_add_event
	} events;
};

#endif
