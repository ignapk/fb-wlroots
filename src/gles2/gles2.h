#ifndef RENDER_GLES2_H
#define RENDER_GLES2_H

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <wlr/backend.h>
#include <wlr/render/egl.h>
#include <wlr/render/interface.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/wlr_texture.h>
#include <wlr/util/log.h>

struct fb_wlr_gles2_pixel_format {
	uint32_t drm_format;
	GLint gl_format, gl_type;
	bool has_alpha;
};

struct fb_wlr_gles2_tex_shader {
	GLuint program;
	GLint proj;
	GLint invert_y;
	GLint tex;
	GLint alpha;
	GLint pos_attrib;
	GLint tex_attrib;
};

struct fb_wlr_gles2_renderer {
	struct wlr_renderer wlr_renderer;

	float projection[9];
	struct wlr_egl *egl;
	int drm_fd;

	const char *exts_str;
	struct {
		bool read_format_bgra_ext;
		bool debug_khr;
		bool egl_image_external_oes;
		bool egl_image_oes;
	} exts;

	struct {
		PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES;
		PFNGLDEBUGMESSAGECALLBACKKHRPROC glDebugMessageCallbackKHR;
		PFNGLDEBUGMESSAGECONTROLKHRPROC glDebugMessageControlKHR;
		PFNGLPOPDEBUGGROUPKHRPROC glPopDebugGroupKHR;
		PFNGLPUSHDEBUGGROUPKHRPROC glPushDebugGroupKHR;
		PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOESPROC glEGLImageTargetRenderbufferStorageOES;
	} procs;

	struct {
		struct {
			GLuint program;
			GLint proj;
			GLint color;
			GLint pos_attrib;
		} quad;
		struct fb_wlr_gles2_tex_shader tex_rgba;
		struct fb_wlr_gles2_tex_shader tex_rgbx;
		struct fb_wlr_gles2_tex_shader tex_ext;
	} shaders;

	struct wl_list buffers; // fb_wlr_gles2_buffer.link
	struct wl_list textures; // fb_wlr_gles2_texture.link

	struct fb_wlr_gles2_buffer *current_buffer;
	uint32_t viewport_width, viewport_height;
};

struct fb_wlr_gles2_buffer {
	struct wlr_buffer *buffer;
	struct fb_wlr_gles2_renderer *renderer;
	struct wl_list link; // fb_wlr_gles2_renderer.buffers

	EGLImageKHR image;
	GLuint rbo;
	GLuint fbo;

	struct wl_listener buffer_destroy;
};

struct fb_wlr_gles2_texture {
	struct wlr_texture wlr_texture;
	struct fb_wlr_gles2_renderer *renderer;
	struct wl_list link; // fb_wlr_gles2_renderer.textures

	// Basically:
	//   GL_TEXTURE_2D == mutable
	//   GL_TEXTURE_EXTERNAL_OES == immutable
	GLenum target;
	GLuint tex;

	EGLImageKHR image;

	bool inverted_y;
	bool has_alpha;

	// Only affects target == GL_TEXTURE_2D
	uint32_t drm_format; // used to interpret upload data
	// If imported from a wlr_buffer
	struct wlr_buffer *buffer;

	struct wl_listener buffer_destroy;
};

const struct fb_wlr_gles2_pixel_format *get_gles2_format_from_drm(uint32_t fmt);
const struct fb_wlr_gles2_pixel_format *get_gles2_format_from_gl(
	GLint gl_format, GLint gl_type, bool alpha);
const uint32_t *get_gles2_shm_formats(size_t *len);

struct fb_wlr_gles2_renderer *gles2_get_renderer(
	struct wlr_renderer *wlr_renderer);
struct fb_wlr_gles2_texture *gles2_get_texture(
	struct wlr_texture *wlr_texture);

struct wlr_texture *gles2_texture_from_pixels(struct wlr_renderer *wlr_renderer,
	uint32_t fmt, uint32_t stride, uint32_t width, uint32_t height,
	const void *data);
struct wlr_texture *gles2_texture_from_wl_drm(struct wlr_renderer *wlr_renderer,
	struct wl_resource *data);
struct wlr_texture *gles2_texture_from_dmabuf(struct wlr_renderer *wlr_renderer,
	struct wlr_dmabuf_attributes *attribs);
struct wlr_texture *gles2_texture_from_buffer(struct wlr_renderer *wlr_renderer,
	struct wlr_buffer *buffer);

void push_gles2_debug_(struct fb_wlr_gles2_renderer *renderer,
	const char *file, const char *func);
#define push_gles2_debug(renderer) push_gles2_debug_(renderer, _WLR_FILENAME, __func__)
void pop_gles2_debug(struct fb_wlr_gles2_renderer *renderer);

struct wlr_renderer *wlr_fbdev_gles2_renderer_create(struct wlr_egl *egl);

struct wlr_egl *fb_wlr_gles2_renderer_get_egl(struct wlr_renderer *renderer);
bool fb_wlr_gles2_renderer_check_ext(struct wlr_renderer *renderer,
	const char *ext);

struct fb_wlr_gles2_texture_attribs {
	GLenum target; /* either GL_TEXTURE_2D or GL_TEXTURE_EXTERNAL_OES */
	GLuint tex;

	bool inverted_y;
	bool has_alpha;
};

bool wlr_texture_is_fb_gles2(struct wlr_texture *texture);
void fb_wlr_gles2_texture_get_attribs(struct wlr_texture *texture,
	struct fb_wlr_gles2_texture_attribs *attribs);
#endif

